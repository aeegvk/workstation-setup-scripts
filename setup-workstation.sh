#!/bin/bash

# Arch / Manjaro
sudo pacman -Syyu
sudo pacman -S yay

# Install Packages
yay -S base-devel docker docker-compose zsh git curl atom phpstorm freeoffice firefox firefox-developer-edition yarn

# Configure Docker
sudo systemctl start docker
sudo systemctl enable docker
sudo usermod -aG docker $USER

# Install Oh My Zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
